from dataclasses import dataclass

from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from flask import g

@dataclass
class Tweet(g.db.Model):
    __tablename__ = 'tweets'

    id: int
    message: str
    user_id: int

    id = g.db.Column(g.db.Integer, primary_key=True, autoincrement=True)
    message = g.db.Column(g.db.String)
    user_id = g.db.Column(g.db.Integer, ForeignKey('users.id'))
    user = relationship('User', back_populates='tweets')

    def __init__(self, message, user_id):
        self.message = message
        self.user_id = user_id

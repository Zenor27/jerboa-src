from dataclasses import dataclass
from sqlalchemy.orm import relationship
from flask import g

@dataclass
class User(g.db.Model):
    __tablename__ = 'users'

    id: int
    username: str
    major: str

    id = g.db.Column(g.db.Integer, primary_key=True, autoincrement=True)
    username = g.db.Column(g.db.String, unique=True)
    major = g.db.Column(g.db.String, nullable=True)

    tweets = relationship('Tweet', back_populates='user')

    def __init__(self, username, major):
        self.username = username
        self.major = major

    def __repr__(self):
        return f'<User id={self.id} username={self.username} major={self.major}>'
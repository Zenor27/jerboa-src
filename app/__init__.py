from flask import g
from flask_sqlalchemy import SQLAlchemy

from app.create_app import create_app

is_testing = False
try:
    is_testing = g.testing
except RuntimeError:
    pass

if not is_testing:
    app = create_app()
    with app.app_context():
        db = SQLAlchemy(app)
        g.db = db
        from app.views import Views

        Views(app, db).run()

import os

from flask import Flask


def create_app():
    app = Flask(__name__)

    try:
        database_address = os.environ['DATABASE_ADDRESS']
        database_username = os.environ['DATABASE_USERNAME']
        database_password = os.environ['DATABASE_PASSWORD']
        database_port = os.environ['DATABASE_PORT']
        database_name = os.environ['DATABASE_NAME']
    except KeyError as e:
        print(f'Error while reading os environ, may be an environment variable is missing: {e}')
        exit(-1)

    db_uri = f'postgresql://{database_username}' \
             f':{database_password}@{database_address}' \
             f':{database_port}/{database_name}'

    app.config['SQLALCHEMY_DATABASE_URI'] = db_uri

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    return app

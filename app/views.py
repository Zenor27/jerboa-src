from flask import jsonify, request

from .models.tweet import Tweet
from .models.user import User


class Views:
    def __init__(self, app, db):
        self.app = app
        self.db = db

    def run(self):
        @self.app.route('/users')
        def get_users():
            return jsonify(User.query.all())

        @self.app.route('/users/<user_id>')
        def get_user(user_id):
            user = User.query.filter_by(id=user_id).first()
            if user is None:
                return f'Sorry, No user matches id {user_id}', 404
            return jsonify(user)

        @self.app.route('/users', methods=['POST'])
        def add_user():
            try:
                data = request.get_json()
                username = data['username']
                major = data['major']
                user = User(username, major)
                self.db.session.add(user)
                self.db.session.commit()
                return '', 200
            except KeyError as e:
                return f'Missing value in body: {e}', 422

        @self.app.route('/tweets')
        def get_tweets():
            return jsonify(Tweet.query.all())

        @self.app.route('/tweets/<tweet_id>')
        def get_tweet(tweet_id):
            tweet = Tweet.query.filter_by(id=tweet_id).first()
            if tweet is None:
                return f'No tweet matches id {tweet_id}', 404
            return jsonify(tweet)

        @self.app.route('/tweets', methods=['POST'])
        def add_tweet():
            try:
                data = request.get_json()
                user_id = data['user_id']
                message = data['message']
                user = User.query.filter_by(id=user_id).first()
                if user is None:
                    return f'No user matches id {user_id}', 404

                tweet = Tweet(message, user_id)
                self.db.session.add(tweet)
                self.db.session.commit()
                return '', 200

            except KeyError as e:
                return f'Missing value in body: {e}', 422

from flask import Flask, g
from flask_sqlalchemy import SQLAlchemy
import pytest


@pytest.fixture(scope="session")
def client():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
    app.config['TESTING'] = True
    with app.test_client() as client:
        with app.app_context():
            g.testing = True
            db = SQLAlchemy(app)
            g.db = db
            from app.views import Views
            Views(app, db).run()
            from app import models
            g.db.create_all()
            yield client
            g.db.drop_all()


def test_get_users_when_none(client):
    assert client.get('/users').get_json() == []


def test_create_user(client):
    user = {
        "username": "Test",
        "major": "MTI"
    }
    client.post('/users', json=user)
    users = client.get('/users').get_json()
    assert users == [{
        "id": users[0]["id"],
        **user
    }]


def test_get_user_with_id(client):
    user = {
        "username": "Test2",
        "major": "MTI"
    }
    client.post('/users', json=user)
    user = client.get('/users/1').get_json()
    assert user is not None


def test_get_tweets_when_none(client):
    assert client.get('/tweets').get_json() == []


def test_create_tweet(client):
    user = {
        "username": "Test3",
        "major": "MTI"
    }
    client.post('/users', json=user)
    tweet = {
        "user_id": 1,
        "message": "Test"
    }
    client.post('/tweets', json=tweet)
    assert len(client.get("/tweets").get_json()) > 0


def test_get_tweet_with_id(client):
    user = {
        "username": "Test4",
        "major": "MTI"
    }
    client.post('/users', json=user)
    tweet = {
        "user_id": 1,
        "message": "Test"
    }
    client.post('/tweets', json=tweet)
    assert client.get("/tweets/1").get_json() is not None

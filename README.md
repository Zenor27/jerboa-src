# jerboa-src

Jerboa source repository.

## How to run everything locally

### Database

This project uses PostgreSQL as database. So you must have a PostgreSQL running on your system.  

### Run project

You must add the right environments variables according to the `.env.default` file. I advise you to create a `.env` file and source it.

```bash
42sh~ python3 -m venv venv
42sh~ source venv/bin/activate
42sh~ pip install -r requirements.txt
42sh~ python3 migrate.py db upgrade # upgrade the database
42sh~ python3 run.py
```
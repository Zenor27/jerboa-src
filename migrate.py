from flask import g
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy

from app import app

if __name__ == '__main__':
    with app.app_context():
        g.db = SQLAlchemy(app)

        migrate = Migrate(app, g.db)

        manager = Manager(app)
        manager.add_command('db', MigrateCommand)
        manager.run()
